﻿using OnlineMenu.Models;
using OnlineMenu.OrderDeliveryPriceCalculator.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineMenu.OrderDeliveryPriceCalculator
{
    public class PercentageOrderCalculator : IOrderDeliveryPriceCalculator
    {
        public decimal CalculateOrder(IEnumerable<Dish> dishes)
        {
            return dishes.Sum(d => d.Price) * 0.1m;
        }
    }
}
