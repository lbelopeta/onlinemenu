﻿
using OnlineMenu.OrderDeliveryPriceCalculator.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.OrderDeliveryPriceCalculator
{
    public class OrderCalculatorFactory : IOrderDeliveryPriceCalculatorFactory
    {
        public IOrderDeliveryPriceCalculator GetCalculator(int calculatorId)
        {
            switch (calculatorId)
            {
                case 1:
                    return new FixedOrderCalculator();
                case 2:
                    return new PercentageOrderCalculator();
                default:
                    return null;
            }
        }
    }
}
