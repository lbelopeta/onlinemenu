﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineMenu.Models
{
    public class Dish : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Column(TypeName = "decimal(8,2)")]
        public decimal Price { get; set; }

        public ICollection<DishIngredient> DishIngredients { get; set; }
    }
}
