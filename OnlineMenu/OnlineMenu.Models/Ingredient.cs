﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMenu.Models
{
    public class Ingredient : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public ICollection<DishIngredient> DishIngredients { get; set; }

    }
}
