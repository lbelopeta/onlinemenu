﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.Models
{
    public class DishIngredient
    {
        public long DishId { get; set; }
        public Dish Dish { get; set; }

        public long IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }
    }
}
