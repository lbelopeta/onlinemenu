﻿using Microsoft.EntityFrameworkCore;
using OnlineMenu.DataAccessLayer.DBContext.EntityTypeConfiguration;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.DataAccessLayer.DBContext
{
    public class OnlineMenuDBContext : DbContext
    {

        public OnlineMenuDBContext(DbContextOptions<OnlineMenuDBContext> options)
           : base(options)
        { }

        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }

        public DbSet<DishIngredient> DishIngredients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DishConfiguration());
            modelBuilder.ApplyConfiguration(new IngredientConfiguration());
            modelBuilder.ApplyConfiguration(new DishIngredientConfiguration());
        }
    }
}
