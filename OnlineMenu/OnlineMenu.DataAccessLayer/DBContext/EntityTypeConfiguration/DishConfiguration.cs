﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.DataAccessLayer.DBContext.EntityTypeConfiguration
{
    public class DishConfiguration : IEntityTypeConfiguration<Dish>
    {
        public void Configure(EntityTypeBuilder<Dish> builder)
        {
            builder.HasIndex(d => d.Name).IsUnique();
        }
    }
}
