﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.DataAccessLayer.DBContext.EntityTypeConfiguration
{
    public class DishIngredientConfiguration : IEntityTypeConfiguration<DishIngredient>
    {
        public void Configure(EntityTypeBuilder<DishIngredient> builder)
        {
            builder.HasKey(di => new { di.DishId,di.IngredientId});

            builder.HasOne(di => di.Ingredient)
                .WithMany(i => i.DishIngredients)
                .HasForeignKey(di => di.IngredientId);

            builder.HasOne(di => di.Dish)
                .WithMany(i => i.DishIngredients)
                .HasForeignKey(di => di.DishId);
        }
    }
}
