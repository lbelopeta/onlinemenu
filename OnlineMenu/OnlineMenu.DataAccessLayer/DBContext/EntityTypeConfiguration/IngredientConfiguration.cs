﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.DataAccessLayer.DBContext.EntityTypeConfiguration
{
    public class IngredientConfiguration : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.HasIndex(i => i.Name).IsUnique();
        }
    }
}
