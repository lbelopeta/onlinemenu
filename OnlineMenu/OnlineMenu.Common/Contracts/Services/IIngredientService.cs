﻿using OnlineMenu.Common.DTO.Ingredients;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.Common.Contracts.Services
{
    public interface IIngredientService
    {
        Task<IEnumerable<Ingredient>> GetAllAsync();
        Task<Ingredient> GetByIdAsync(long id);
        Task CreateAsync(CreateIngredientRequest createIngredientRequest);
        Task UpdateAsync(long id,CreateIngredientRequest createIngredientRequest);
        Task DeleteAsync(long id);
    }
}
