﻿using OnlineMenu.Common.DTO.Order;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.Common.Contracts.Services
{
    public interface IOrderService
    {
        Task<OrderResponse> CreateOrder(CreateOrderRequest createOrderRequest);
    }
}
