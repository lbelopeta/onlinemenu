﻿using OnlineMenu.Common.DTO.Dishes;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.Common.Contracts.Services
{
    public interface IDishService
    {
        Task<IEnumerable<Dish>> GetAllAsync();
        Task<Dish> GetByIdAsync(long id);
        Task CreateAsync(CreateDishRequest createDishRequest);
        Task UpdateAsync(long id, CreateDishRequest createDishRequest);
        Task DeleteAsync(long id);
    }
}
