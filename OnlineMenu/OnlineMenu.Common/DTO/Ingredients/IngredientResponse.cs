﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.Common.DTO.Ingredients
{
    public class IngredientResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
