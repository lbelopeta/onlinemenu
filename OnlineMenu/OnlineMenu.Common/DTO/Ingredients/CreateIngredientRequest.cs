﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMenu.Common.DTO.Ingredients
{
    /// <summary>
    /// Model used for creating and updateing Ingredient
    /// </summary>
    public class CreateIngredientRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
