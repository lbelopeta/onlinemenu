﻿using OnlineMenu.Common.DTO.Dishes;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.Common.DTO.Order
{
    public class OrderResponse
    {
        public IEnumerable<DishResponse> Dishes { get; set; }

        public decimal Price { get; set; }

        public decimal DeliveryPrice { get; set; }
    }
}
