﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMenu.Common.DTO.Order
{
    public class CreateOrderRequest
    {
        [Required]
        public int? CalculationType { get; set; }
        [Required]
        public IEnumerable<long> RequestDishes { get; set; }
    }
}
