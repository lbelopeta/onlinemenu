﻿using OnlineMenu.Common.DTO.Ingredients;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.Common.DTO.Dishes
{
    public class DishResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public decimal Price { get; set; }

        public ICollection<IngredientResponse> Ingredients { get; set; }
    }
}
