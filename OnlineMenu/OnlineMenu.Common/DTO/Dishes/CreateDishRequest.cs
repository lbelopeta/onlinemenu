﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMenu.Common.DTO.Dishes
{
    public class CreateDishRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public decimal? Price { get; set; }

        [Required]
        public ICollection<long> DishIngredients { get; set; }
    }
}
