﻿using AutoMapper;
using OnlineMenu.Common.DTO.Dishes;
using OnlineMenu.Common.DTO.Ingredients;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineMenu.Common.Automapper
{
    public class OnlineMenuProfile : Profile
    {
        public OnlineMenuProfile()
        {
            CreateMap<CreateIngredientRequest, Ingredient>(MemberList.Source);
            CreateMap<Ingredient, IngredientResponse>(MemberList.Destination);

            CreateMap<CreateDishRequest, Dish>(MemberList.Source)
                .ForMember(dest => dest.DishIngredients, opt => opt.Ignore());

            CreateMap<Dish, DishResponse>(MemberList.Source)
                .ForMember(dest => dest.Ingredients, opt => opt.MapFrom(src => src.DishIngredients.Select(di => di.Ingredient)));
        }
    }
}
