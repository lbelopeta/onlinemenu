﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OnlineMenu.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnlineMenu.Api.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly JsonSerializerSettings _jsonSerializerSettings;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                },
                Formatting = Formatting.Indented
            };

        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ResourceNotFoundException ex)
            {
                await HandleExceptionAsync(httpContext, ex, HttpStatusCode.NotFound, "Request resource was not found");
            }
            catch (BusinessException ex)
            {
                await HandleExceptionAsync(httpContext, ex, HttpStatusCode.BadRequest, ex.Message);
            }
            catch (DbUpdateException ex)
            {
                if (ex?.InnerException is SqlException sqlEx)
                {

                    await HandleExceptionAsync(httpContext, ex, HttpStatusCode.BadRequest, sqlEx.Message);
                }
                else
                {
                    await HandleExceptionAsync(httpContext, ex, HttpStatusCode.BadRequest, "Database error");
                }
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex, HttpStatusCode.InternalServerError, "Internal server error");
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception, HttpStatusCode httpStatusCode, string message)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            string json = JsonConvert.SerializeObject(new { Errors = message}, _jsonSerializerSettings);
            return context.Response.WriteAsync(json);
        }


    }
}
