﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using OnlineMenu.DataAccessLayer.DBContext;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineMenu.Api.DBContextFactory
{
    public class OnlineMenuContextFactory : IDesignTimeDbContextFactory<OnlineMenuDBContext>
    {
        public OnlineMenuDBContext CreateDbContext(string[] args)
        {
            string envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{envName}.json", true)
                .Build();

            string connectionString = configuration.GetConnectionString("OnlineMenuDatabase");


            var optionsBuilder = new DbContextOptionsBuilder<OnlineMenuDBContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new OnlineMenuDBContext(optionsBuilder.Options);
        }
    }
}
