﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OnlineMenu.Api.Filters
{
    public class NotFoundFilter : ResultFilterAttribute
    {
        public override Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if(context.Result is ObjectResult response && response.Value == null)
            {
                response = new NotFoundObjectResult(null);
                response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Result = response;
            }

            return base.OnResultExecutionAsync(context, next);
        }

    }
}
