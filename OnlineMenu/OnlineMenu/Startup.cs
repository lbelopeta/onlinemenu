using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OnlineMenu.DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using OnlineMenu.Common.Automapper;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.BusinessLayer.Services;
using OnlineMenu.Api.Middlewares;
using OnlineMenu.Api.Filters;

namespace OnlineMenu
{
    public class Startup
    {
        private const string CONNECTION_STRING_KEY = "OnlineMenuDatabase";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
                options.Filters.Add(new ValidateModelAttribute());
                options.Filters.Add(new NotFoundFilter());
            });
            string connectionString = Configuration.GetConnectionString(CONNECTION_STRING_KEY);
            services.AddDbContext<OnlineMenuDBContext>(options => options.UseSqlServer(connectionString));
            services.AddAutoMapper(typeof(OnlineMenuProfile));

            services.AddScoped<IIngredientService, IngredientService>();
            services.AddScoped<IDishService, DishService>();
            services.AddScoped<IOrderService, OrderService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMvc();
        }
    }
}
