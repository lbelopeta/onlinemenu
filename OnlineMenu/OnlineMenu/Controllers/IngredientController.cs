﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Ingredients;
using OnlineMenu.Models;

namespace OnlineMenu.Api.Controllers
{
    public class IngredientController : OnlineMenuBaseController
    {
        private readonly IIngredientService _serivce;

        public IngredientController(IIngredientService ingredientService, IMapper mapper) : base(mapper)
        {
            _serivce = ingredientService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<Ingredient> ingredients = await _serivce.GetAllAsync();
            IEnumerable<IngredientResponse> ingredientResponses = _mapper.Map<IEnumerable<Ingredient>, IEnumerable<IngredientResponse>>(ingredients);
            return Ok(ingredientResponses);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAll(long id)
        {
            Ingredient ingredient = await _serivce.GetByIdAsync(id);
            IngredientResponse ingredientResponse = _mapper.Map<Ingredient, IngredientResponse>(ingredient);
            return Ok(ingredientResponse);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateIngredientRequest createIngredientRequest)
        {
            await _serivce.CreateAsync(createIngredientRequest);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody]CreateIngredientRequest createIngredientRequest)
        {
            await _serivce.UpdateAsync(id, createIngredientRequest);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _serivce.DeleteAsync(id);
            return Ok();
        }
    }
}