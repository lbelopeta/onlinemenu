﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Order;

namespace OnlineMenu.Api.Controllers
{
    public class OrderController : OnlineMenuBaseController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService, IMapper mapper) : base(mapper)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder([FromBody]CreateOrderRequest createOrderRequest)
        {
            var orderData = await _orderService.CreateOrder(createOrderRequest);
            return Ok(orderData);
        }
    }
}