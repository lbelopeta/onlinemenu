﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OnlineMenu.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public abstract class OnlineMenuBaseController : ControllerBase
    {
        protected readonly IMapper _mapper;

        public OnlineMenuBaseController(IMapper mapper)
        {
            _mapper = mapper;
        }
    }
}