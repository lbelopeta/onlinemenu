﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Dishes;
using OnlineMenu.Models;

namespace OnlineMenu.Api.Controllers
{
    public class DishController : OnlineMenuBaseController
    {

        private readonly IDishService _serivce;

        public DishController(IDishService dishService, IMapper mapper) : base(mapper)
        {
            _serivce = dishService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<Dish> dishes = await _serivce.GetAllAsync();
            IEnumerable<DishResponse> dishResponses = _mapper.Map<IEnumerable<Dish>, IEnumerable<DishResponse>>(dishes);
            return Ok(dishResponses);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAll(long id)
        {
            Dish dish = await _serivce.GetByIdAsync(id);
            DishResponse ingredientResponse = _mapper.Map<Dish, DishResponse>(dish);
            return Ok(ingredientResponse);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateDishRequest createDishRequest)
        {
            await _serivce.CreateAsync(createDishRequest);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody]CreateDishRequest createDishRequest)
        {
            await _serivce.UpdateAsync(id, createDishRequest);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await _serivce.DeleteAsync(id);
            return Ok();
        }
    }
}