﻿using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.OrderDeliveryPriceCalculator.Common
{
    public interface IOrderDeliveryPriceCalculator
    {
        decimal CalculateOrder(IEnumerable<Dish> dishes);
    }
}
