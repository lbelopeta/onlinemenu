﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMenu.OrderDeliveryPriceCalculator.Common
{
    public interface IOrderDeliveryPriceCalculatorFactory
    {
        IOrderDeliveryPriceCalculator GetCalculator(int calculatorId);
    }
}
