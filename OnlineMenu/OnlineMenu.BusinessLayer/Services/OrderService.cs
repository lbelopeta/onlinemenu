﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Dishes;
using OnlineMenu.Common.DTO.Order;
using OnlineMenu.Common.Exceptions;
using OnlineMenu.DataAccessLayer.DBContext;
using OnlineMenu.Models;
using OnlineMenu.OrderDeliveryPriceCalculator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.BusinessLayer.Services
{
    public class OrderService : IOrderService, IDisposable
    {
        private readonly OnlineMenuDBContext _context;
        private readonly IMapper _mapper;
        private AssemblyLoadContext _assemblyLoadContext;

        public OrderService(OnlineMenuDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OrderResponse> CreateOrder(CreateOrderRequest createOrderRequest)
        {
            List<Dish> dishes = await _context.Dishes
                .AsNoTracking()
                .Include(d => d.DishIngredients)
                .ThenInclude(di => di.Ingredient)
                .Where(d => createOrderRequest.RequestDishes.Contains(d.Id))
                .ToListAsync();

            OrderResponse orderResponse = new OrderResponse();
            orderResponse.Dishes = _mapper.Map<List<Dish>, List<DishResponse>>(dishes);
            orderResponse.Price = dishes.Sum(d => d.Price);

            IOrderDeliveryPriceCalculator caluculator = GetDeliveryPriceCalculator(createOrderRequest.CalculationType.Value);

            orderResponse.DeliveryPrice = caluculator.CalculateOrder(dishes);
            return orderResponse;
        }

        private IOrderDeliveryPriceCalculator GetDeliveryPriceCalculator(int caluculationType)
        {
            Assembly assembly = LoadPlugin(@"OnlineMenu.OrderCalculator\bin\Debug\netstandard2.0\OnlineMenu.OrderDeliveryPriceCalculator.dll");
            IOrderDeliveryPriceCalculatorFactory factory = GetFactory(assembly);

            if(factory == null)
            {
                throw new BusinessException("Please contact administration");
            }

            IOrderDeliveryPriceCalculator orderDeliveryPriceCalculator = factory.GetCalculator(caluculationType);

            if(orderDeliveryPriceCalculator == null)
            {
                throw new BusinessException("Please contact administration");
            }

            return orderDeliveryPriceCalculator;

        }

        Assembly LoadPlugin(string relativePath)
        {
            // Navigate up to the solution root
            string root = Path.GetFullPath(Path.Combine(
                Path.GetDirectoryName(
                    Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Path.GetDirectoryName(
                                Path.GetDirectoryName(typeof(OrderService).Assembly.Location)))))));

            string pluginLocation = Path.GetFullPath(Path.Combine(root, relativePath.Replace('\\', Path.DirectorySeparatorChar)));
            Console.WriteLine($"Loading commands from: {pluginLocation}");
            _assemblyLoadContext = new AssemblyLoadContext(pluginLocation,true);
            Assembly assembly = null;
            using (var fs = new FileStream(pluginLocation, FileMode.Open, FileAccess.Read))
            {
                assembly = _assemblyLoadContext.LoadFromStream(fs);
            }


            return assembly;
        }

        IOrderDeliveryPriceCalculatorFactory GetFactory(Assembly assembly) {
            IOrderDeliveryPriceCalculatorFactory factory = null;
            foreach (Type type in assembly.GetTypes())
            {
                if (typeof(IOrderDeliveryPriceCalculatorFactory).IsAssignableFrom(type))
                {
                    IOrderDeliveryPriceCalculatorFactory result = Activator.CreateInstance(type) as IOrderDeliveryPriceCalculatorFactory;
                    if (result != null)
                    {
                        factory = result;
                        break;
                    }
                }
            }
            return factory;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_assemblyLoadContext != null)
                    {
                        _assemblyLoadContext.Unload();
                    }
                }

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);

        }
        #endregion
    }
}
