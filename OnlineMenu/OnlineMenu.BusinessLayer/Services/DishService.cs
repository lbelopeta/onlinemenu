﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Dishes;
using OnlineMenu.Common.Exceptions;
using OnlineMenu.DataAccessLayer.DBContext;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.BusinessLayer.Services
{
    public class DishService : IDishService
    {
        private readonly OnlineMenuDBContext _context;
        private readonly IMapper _mapper;

        public DishService(OnlineMenuDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateDishRequest createDishRequest)
        {
            Dish dish = _mapper.Map<CreateDishRequest, Dish>(createDishRequest);
            dish.DishIngredients = new List<DishIngredient>();
            foreach(var ingredientId in createDishRequest.DishIngredients)
            {
                dish.DishIngredients.Add(new DishIngredient { IngredientId = ingredientId });
            }
            _context.Dishes.Attach(dish);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            Dish dish = await _context.Dishes.FirstOrDefaultAsync(i => i.Id == id);

            if (dish == null)
            {
                throw new ResourceNotFoundException();
            }

            _context.Remove(dish);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Dish>> GetAllAsync()
        {
            IEnumerable<Dish> dishes = await _context.Dishes
               .AsNoTracking()
               .Include(d=> d.DishIngredients)
               .ThenInclude(di => di.Ingredient)
               .ToListAsync();

            return dishes;
        }

        public async Task<Dish> GetByIdAsync(long id)
        {
            Dish dish = await _context.Dishes
               .AsNoTracking()
               .Include(d => d.DishIngredients)
               .ThenInclude(di => di.Ingredient)
               .FirstOrDefaultAsync(i => i.Id == id);

            return dish;
        }

        public async Task UpdateAsync(long id, CreateDishRequest createDishRequest)
        {
            Dish dish = await _context.Dishes
                .Include(d => d.DishIngredients)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (dish == null)
            {
                throw new ResourceNotFoundException();
            }


            dish.Name = createDishRequest.Name;
            dish.Price = createDishRequest.Price.Value;

            dish.DishIngredients.Clear();
            foreach (var ingredientId in createDishRequest.DishIngredients)
            {
                dish.DishIngredients.Add(new DishIngredient { IngredientId = ingredientId });
            }
            await _context.SaveChangesAsync();
        }
    }
}
