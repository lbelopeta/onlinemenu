﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using OnlineMenu.Common.Contracts.Services;
using OnlineMenu.Common.DTO.Ingredients;
using OnlineMenu.Common.Exceptions;
using OnlineMenu.DataAccessLayer.DBContext;
using OnlineMenu.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMenu.BusinessLayer.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly OnlineMenuDBContext _context;
        private readonly IMapper _mapper;

        public IngredientService(OnlineMenuDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateIngredientRequest createIngredientRequest)
        {
            Ingredient ingredient = _mapper.Map<CreateIngredientRequest, Ingredient>(createIngredientRequest);
            _context.Add(ingredient);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            Ingredient ingredient = await _context.Ingredients.FirstOrDefaultAsync(i => i.Id == id);

            if(ingredient == null)
            {
                throw new ResourceNotFoundException();
            }

            _context.Remove(ingredient);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Ingredient>> GetAllAsync()
        {
            IEnumerable<Ingredient> ingredients = await _context.Ingredients
                .AsNoTracking()
                .ToListAsync();

            return ingredients;
        }

        public async Task<Ingredient> GetByIdAsync(long id)
        {
            Ingredient ingredient = await _context.Ingredients
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.Id == id);

            return ingredient;
        }

        public async Task UpdateAsync(long id, CreateIngredientRequest createIngredientRequest)
        {
            Ingredient ingredient = await _context.Ingredients.FirstOrDefaultAsync(i => i.Id == id);

            if (ingredient == null)
            {
                throw new ResourceNotFoundException();
            }

            _mapper.Map(createIngredientRequest, ingredient);
            await _context.SaveChangesAsync();
        }
    }
}
